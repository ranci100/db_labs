#include "sqlite3.h"
#include <string>
#include <iostream>
#include <io.h>

bool sendStatement(const char* sqlStatement, char* errMessage, sqlite3* db, int& res);

int main(void)
{
	sqlite3* db;
	std::string dbFileName = "MyDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK) 
	{
		db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return -1;
	}
	if (doesFileExist == 0) 
	{
		char* errMessage = nullptr;
		sendStatement("CREATE TABLE PERSONS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, LAST_NAME TEXT NOT NULL, FIRST_NAME TEXT NOT NULL, EMAIL TEXT NOT NULL);", errMessage, db, res);
		sendStatement("CREATE TABLE PhonePrefix (phonePrefixID INTEGER PRIMARY KEY AUTOINCREMENT, Prefix INTEGER);", errMessage, db, res);
		sendStatement("CREATE TABLE Phone (phoneID INTEGER PRIMARY KEY AUTOINCREMENT, FOREIGN KEY(phonePrefixID) REFERENCES PhomePrefix(phonePrefixID) , phoneNumber INTEGER, FOREIGN KEY(PersonID) REFERENCES Persons(phonePrefixID));", errMessage, db, res);
	}
	sqlite3_close(db);
	db = nullptr;

	return 0;
}

/*
	the function get statment and db and send the statment to the db
*/
bool sendStatement(const char * sqlStatement, char* errMessage , sqlite3* db, int& res)
{
	res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		return false;
}